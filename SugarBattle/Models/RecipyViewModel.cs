﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SugarBattle.Models
{
    public class RecipyViewModel
    {
        public int RecipyId { get; set; }

        public string RecipyName { get; set; }

        public string Ingredients { get; set; }

        public string Instructions { get; set; }

        public string Nutrition { get; set; }

        public int Time { get; set; }

        public Nullable<int> Serves { get; set; }

        public Boolean saved { get; set; }
    }
}