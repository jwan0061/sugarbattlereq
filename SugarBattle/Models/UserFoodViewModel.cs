﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SugarBattle.Models
{
    public class UserFoodViewModel
    {

        public int UserId { get; set; }

        public Nullable<System.DateTime> FoodDate { get; set; }
        public decimal FoodAmountTotal { get; set; }
        public decimal SugarAmountTotal { get; set; }

        public List<FoodViewModel> UserFoodList { get; set; }
    }
}