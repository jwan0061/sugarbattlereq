
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace SugarBattle.Models
{

using System;
    using System.Collections.Generic;
    
public partial class Food
{

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
    public Food()
    {

        this.TempFoodGain = new HashSet<TempFoodGain>();

        this.UserFood = new HashSet<UserFood>();

    }


    public int food_Id { get; set; }

    public string food_name { get; set; }

    public Nullable<int> food_type { get; set; }

    public Nullable<decimal> sugar { get; set; }

    public Nullable<decimal> quantity { get; set; }



    public virtual FoodType FoodType { get; set; }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]

    public virtual ICollection<TempFoodGain> TempFoodGain { get; set; }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]

    public virtual ICollection<UserFood> UserFood { get; set; }

}

}
