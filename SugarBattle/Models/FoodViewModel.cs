﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SugarBattle.Models
{
    public class FoodViewModel
    {

        public int food_Id { get; set; }
        public string food_name { get; set; }
        public Nullable<decimal> quantity { get; set; }
        public Nullable<int> food_type { get; set; }
        public Nullable<decimal> sugar { get; set; }
        public Nullable<decimal> food_number { get; set; }
        public decimal user_temp_id { get; set; }

        public int food_type_ID { get; set; }
        public string type_description { get; set; }
    }
}