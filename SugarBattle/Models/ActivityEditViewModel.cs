﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SugarBattle.Models
{
    public class ActivityEditViewModel
    {
        public int Id { get; set; }

        public int LocationId { get; set; }

        public string LocationStart { get; set; }

        public string LocationEnd { get; set; }

        public string LocationDate { get; set; }

        public string LocationDescription { get; set; }

        public string Description { get; set; }

        public string LocationName { get; set; }
    }
}