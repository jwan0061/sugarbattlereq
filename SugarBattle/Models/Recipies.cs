
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace SugarBattle.Models
{

using System;
    using System.Collections.Generic;
    
public partial class Recipies
{

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
    public Recipies()
    {

        this.UserRecipies = new HashSet<UserRecipies>();

    }


    public int RecipyId { get; set; }

    public string RecipyName { get; set; }

    public string Ingredients { get; set; }

    public string Instructions { get; set; }

    public string Nutrition { get; set; }

    public int Time { get; set; }

    public Nullable<int> Serves { get; set; }



    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]

    public virtual ICollection<UserRecipies> UserRecipies { get; set; }

}

}
