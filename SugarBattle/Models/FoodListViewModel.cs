﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SugarBattle.Models
{
    public class FoodListViewModel
    {
        public int food_type_ID { get; set; }
        public string type_description { get; set; }

        public List<FoodViewModel> foodViews { get; set; }
    }
}