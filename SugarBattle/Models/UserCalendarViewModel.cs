﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SugarBattle.Models
{
    public class UserCalendarViewModel
    {
        public int Id { get; set; }

        public int LocationId { get; set; }

        public System.DateTime LocationStart { get; set; }

        public System.DateTime LocationEnd { get; set; }

        public string Description { get; set; }

        public string LocationDescription { get; set; }

        public string LocationName { get; set; }

        public string locationType { get; set; }
    }
}