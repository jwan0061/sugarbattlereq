﻿var pos = { lat: -37.877428, lng: 145.044209 };
var map = new google.maps.Map(document.getElementById("activity-map"), {
    zoom: 15,
    center: pos,
    mapTypeId: google.maps.MapTypeId.ROADMAP,

    zoomControlOptions: {
        position: google.maps.ControlPosition.RIGHT_BOTTOM
    },

    streetViewControl: true,

    streetViewControlOptions: {
        position: google.maps.ControlPosition.RIGHT_BOTTOM
    },

    mapTypeControlOptions: {
        position: google.maps.ControlPosition.LEFT_TOP
    },
});

$("#locationList").load("/Location/FindNearParks?lat=" + map.getCenter().lat() + "&lng=" + map.getCenter().lng());

var trafficLayer = new google.maps.TrafficLayer();
trafficLayer.setMap(map);

var input = document.getElementById("pac-input");
var button = document.getElementById("searchbtn");

var searchBox = new google.maps.places.SearchBox(input);



var markers = [];

searchBox.addListener('places_changed', function () {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
        return;
    }

    markers.forEach(function (marker) {
        marker.setMap(null);
    });

    markers = [];

    var bounds = new google.maps.LatLngBounds();

    places.forEach(function (place) {
        if (!place.geometry) {
            return;
        }
        var icon = {
            url: place.icon,
            size: new google.maps.Size(500, 500),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(40, 40)
        };

        var newMarker = new google.maps.Marker({
            map: map,
            icon: icon,
            title: place.name,
            position: place.geometry.location
        });

        markers.push(newMarker);

        if (place.geometry.viewpoint) {
            bounds.union(place.geometry.viewpoint);
        } else {
            bounds.extend(place.geometry.location);
        }
    });
    map.fitBounds(bounds);
    map.setZoom(14);
    $("#locationList").load("/Location/FindNearParks?lat=" + map.getCenter().lat() + "&lng=" + map.getCenter().lng());
});


var parkMarkers = []
var infowindow;
function placeMarker(location, title, desc, type, locationId) {
    var image = "";
    if (type == "Park") {
        image = "../img/location/park-icon.png";
    } else if (type == "Playground") {
        image = "../img/location/playground-icon.png";
    }
    var icon = {
        url: image,
        scaledSize: new google.maps.Size(70, 60),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(0, 0)
    };
    marker = new google.maps.Marker({
        position: location,
        draggable: false,
        title: title,
        icon: icon
    });
    parkMarkers.push(marker);

    marker.setMap(map);

    showinfomessage(marker, title, desc, locationId);
};

function showinfomessage(marker, title, desc, locationId) {
    infowindow = new google.maps.InfoWindow({
        content: "<div class ='text-info' style='font-size: medium'>" + title +
            "</div><div>" + desc + "</div><div><button href='#' onclick='addActivity(" + locationId
            + ")' + class= 'btn btn-info btn-block'> Add to My Calendar</button> " + "</div> "
    });
    google.maps.event.addListener(marker, 'click', function () {
        infowindow.open(map, marker);
    });
};


var submitActivity = function () {
    var locationId = $("#LocationId").val();
    var date = $("#activityTime").val();
    var start = $("#activityStart").val();
    var end = $("#activityEnd").val();
    var desc = $("#activityDesc").val();
    var activityId = $("#ActivityId").val();
    var data = { LocationId: locationId, Desc: desc, Date: date, Start: start, End: end, ActivityId: activityId };
    $.ajax({
        type: "POST",
        url: "/Location/AddToCalendar",
        data: data,
        async: false,
        success: function (response) {
            if (response == "success") {
                $.notify({
                    icon: 'glyphicon glyphicon-thumbs-up',
                    message: 'You have successfully create one new activity',

                }, {
                        type: 'success',
                        delay: 5000
                    });
                $("#addActivityModal").modal("hide");
                $("#locationArea").load("/Location/UserCalendar");
                $("#ActivityId").val(''); 
            } else {
                $.notify({
                    icon: 'glyphicon glyphicon-warning-sign',
                    message: response,
                    target: '_self'
                }, {
                        type: 'danger',
                        delay: 5000,
                        placement: {
                            from: "bottom",
                            align: "right"
                        }
                    });
            }
            
        }
    })
}

var cancelActivity = function () {
    var activityId = $("#ActivityId").val();
    var data = { LocationId: activityId };
    $.ajax({
        type: "POST",
        url: "/Location/CancelFromCalendar",
        data: data,
        success: function (response) {
            if (response == "success") {
                $.notify({
                    icon: 'glyphicon glyphicon-thumbs-up',
                    message: 'You have successfully cancel one activity',

                }, {
                        type: 'info',
                        delay: 5000
                    });
                $("#addActivityModal").modal("hide");
                $("#locationArea").load("/Location/UserCalendar");
                $("#ActivityId").val('');
            } 
        }
    })
}

var locations = [];

$(".locations").each(function () {
    var locationId = $(".locationId", this).text().trim();
    var name = $(".name", this).text().trim();
    var desc = $(".desc", this).text().trim();
    var type = $(".locationType", this).text().trim();
    var latitude = $(".latitude", this).text().trim();
    var longtitude = $(".longtitude", this).text().trim();

    var location = {
        locationId: locationId,
        name: name,
        desc: desc,
        locationType: type,
        lat: parseFloat(latitude),
        lng: parseFloat(longtitude)
    };

    locations.push(location);
});

for (var i = 0; i < locations.length; i++) {
    placeMarker({ lat: locations[i]["lat"], lng: locations[i]["lng"] }, locations[i]["name"], locations[i]["desc"], locations[i]["locationType"], locations[i]["locationId"]);
}

var getMarkInf = function (locationId, title, dec) {
    infowindow.close();
    infowindow = new google.maps.InfoWindow({
        content: "<div class ='text-info' style='font-size: medium'>" + title +
            "</div><div>" + dec + "</div><div><button href='#' onclick='addActivity(" + locationId
            + ")' + class= 'btn btn-info btn-block'> Add to My Calendar</button> " + "</div> "
    });
 
    for (var i = 0; i < locations.length; i++) {
        if (parkMarkers[i].title == title) {
            infowindow.open(map, parkMarkers[i]);
            map.setCenter(parkMarkers[i].position);

        }
    }
}
