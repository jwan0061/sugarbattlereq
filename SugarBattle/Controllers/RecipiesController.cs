﻿using SugarBattle.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SugarBattle.Controllers
{
    public class RecipiesController : Controller
    {
        // GET: Recipies
        public ActionResult Index()
        {
            sugarbattleEntities db = new sugarbattleEntities();

            List<RecipyViewModel> recipies = db.Recipies.Select(x => new RecipyViewModel
            {
                RecipyId = x.RecipyId,
                Ingredients = x.Ingredients,
                Instructions = x.Instructions,
                Nutrition = x.Nutrition,
                RecipyName = x.RecipyName,
                Time = x.Time,
                Serves = x.Serves,
                saved = false
            }).ToList();

            if (Session["user"] != null)
            {
                foreach (var item in recipies)
                {
                    int userId = (int)Session["user"];
                    UserRecipies userRecipies = db.UserRecipies.SingleOrDefault(x => x.RecipyId == item.RecipyId && x.UserId == userId);
                    if (userRecipies != null)
                    {
                        item.saved = true;
                    }
                }

            }

            return View(recipies);
        }

        public ActionResult ShowUserRecipy()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "Account");
            } else
            {
                int userId = (int)Session["user"];
                sugarbattleEntities db = new sugarbattleEntities();
                List<RecipyViewModel> recipyViewModels = db.UserRecipies.Where(x => x.UserId == userId).Select(y => new RecipyViewModel
                {
                    RecipyId = y.RecipyId,
                    Ingredients = y.Recipies.RecipyName,
                    Instructions = y.Recipies.Instructions,
                    Nutrition = y.Recipies.Nutrition,
                    RecipyName = y.Recipies.RecipyName,
                    Serves = y.Recipies.Serves,
                    Time = y.Recipies.Time,
                    saved = true
                }).ToList();
                return View("Index", recipyViewModels);
            }
        }

        public JsonResult SaveRecipy(int RecipyId)
        {
            string response = "";
            if (Session["user"] == null)
            {
                response = "false";
            } else
            {
                sugarbattleEntities db = new sugarbattleEntities();

                int userId = (int)Session["user"];
                UserRecipies userRecipies = new UserRecipies();
                userRecipies.UserId = userId;
                userRecipies.RecipyId = RecipyId;
                db.UserRecipies.Add(userRecipies);
                db.SaveChanges();
                response = "success";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CancelRecipy(int RecipyId)
        {
            sugarbattleEntities db = new sugarbattleEntities();
            int userId = (int)Session["user"];
            UserRecipies userRecipies = db.UserRecipies.SingleOrDefault(x => x.RecipyId == RecipyId && x.UserId == userId);
            db.UserRecipies.Remove(userRecipies);

            db.SaveChanges();
            return Json("success", JsonRequestBehavior.AllowGet);
        }
    }
}