﻿using SugarBattle.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SugarBattle.Controllers
{
    public class DailyPlanController : Controller
    {
        // GET: SugarCalculate
        public ActionResult CalculateSugar()
        {
            sugarbattleEntities db = new sugarbattleEntities();
            if (Session["tempUserId"] == null)
            {
                Random random = new Random();
                Session["tempUserId"] = random.Next(1, 100000000);
            } 

            //List<TempFoodGain> tempFoodGains = db.TempFoodGain.ToList();
            //db.TempFoodGain.RemoveRange(tempFoodGains);
            //db.SaveChanges();

            //List<FoodType> foodTypes = db.FoodType.Where(x => x.type_description == "temp").ToList();
            //db.FoodType.RemoveRange(foodTypes);
            //db.SaveChanges();

            List<FoodType> foodTypeList = db.FoodType.ToList();

            List<FoodListViewModel> foodListViewList = foodTypeList.Select(y => new FoodListViewModel
            {
                food_type_ID = y.food_type_ID,
                type_description = y.type_description,

            }).ToList();

            return View(foodListViewList);
        }

        [HttpPost]
        public JsonResult SearchItem(string SearchContent, int FoodTypeId)
        {
            sugarbattleEntities db = new sugarbattleEntities();
            FoodViewModel foodListModel = new FoodViewModel();
            List<FoodViewModel> foodViewList = new List<FoodViewModel>();

            if (FoodTypeId == 0)
            {
                foodViewList = db.Food.Where(x => x.food_name.Contains(SearchContent) && x.FoodType != null).Select(x => new FoodViewModel
                {
                    food_Id = x.food_Id,
                    food_name = x.food_name,
                    quantity = x.quantity,
                    sugar = x.sugar,
                    food_type_ID = x.FoodType.food_type_ID,
                    type_description = x.FoodType.type_description
                }).ToList();
            }
            else
            {
                foodViewList = db.Food.Where(x => x.food_name.Contains(SearchContent) && x.food_type == FoodTypeId).Select(x => new FoodViewModel
                {
                    food_Id = x.food_Id,
                    food_name = x.food_name,
                    quantity = x.quantity,
                    sugar = x.sugar,
                    food_type_ID = x.FoodType.food_type_ID,
                    type_description = x.FoodType.type_description
                }).ToList();
            }
            if (foodViewList.Count == 0)
            {
                string result = "false";
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //foreach (var item in foodViewList)
                //{
                //    TempFoodGain tempFoodGain = db.TempFoodGain.SingleOrDefault(x => x.food_Id == item.food_Id);
                //    if (tempFoodGain == null)
                //    {
                //        item.food_number = 0;
                //    }
                //    else
                //    {
                //        item.food_number = tempFoodGain.food_number;
                //    }
                //}
                return Json(foodViewList, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ShowFoodList(string foodTypeId)
        {
            sugarbattleEntities db = new sugarbattleEntities();
            FoodViewModel foodListModel = new FoodViewModel();
            int type_id = int.Parse(foodTypeId);
            List<FoodViewModel> foodViewList = new List<FoodViewModel>();

            if (type_id == 0)
            {
                foodViewList = db.Food.Where(x => x.food_type != null).Select(x => new FoodViewModel
                {
                    food_Id = x.food_Id,
                    food_name = x.food_name,
                    quantity = x.quantity,
                    sugar = x.sugar,
                    food_type_ID = x.FoodType.food_type_ID,
                    type_description = x.FoodType.type_description
                }).ToList();
            } else if (type_id == -1)
            {
                int userId = (int)Session["user"];
                
                foodViewList = db.UserFood.Where(x => x.UserId == userId && x.Food.food_type == null).Select(y => new FoodViewModel
                {
                    food_Id = y.FoodId,
                    food_name = y.Food.food_name,
                    quantity = y.Food.quantity,
                    sugar = y.Food.sugar,
                    food_type_ID = -1,
                    type_description = "userFood"
                }).Distinct().ToList();
            } else
            {
                foodViewList = db.Food.Where(x => x.food_type == type_id && x.food_type != null).Select(x => new FoodViewModel
                {
                    food_Id = x.food_Id,
                    food_name = x.food_name,
                    quantity = x.quantity,

                    sugar = x.sugar,
                    food_type_ID = x.FoodType.food_type_ID,
                    type_description = x.FoodType.type_description
                }).ToList();
            }
           

            ViewBag.FoodTypeId = foodTypeId;

            return View(foodViewList);
        }

        [HttpPost]
        public JsonResult AddItem(decimal FoodNumber, int FoodId)
        {

            String result = "fail";
            try
            {
                sugarbattleEntities db = new sugarbattleEntities();
                int tempUserId = (int)Session["tempUserId"];
                TempFoodGain tempFoodGain = db.TempFoodGain.SingleOrDefault(x => x.food_Id == FoodId && x.user_temp_id == tempUserId);
               
                if (tempFoodGain == null)
                {
                    tempFoodGain = new TempFoodGain();
                    tempFoodGain.food_Id = FoodId;
                    tempFoodGain.food_number = FoodNumber;
                    tempFoodGain.user_temp_id = (int)(Session["tempUserId"]);
                    db.TempFoodGain.Add(tempFoodGain);
                }
                else
                {
                    tempFoodGain.food_number = tempFoodGain.food_number + FoodNumber;
                }
                if (tempFoodGain.food_number == 0)
                {
                    db.TempFoodGain.Remove(tempFoodGain);
                }
                if (tempFoodGain.food_number < 0)
                {

                }
                db.SaveChanges();
                result = "success";
            } catch(Exception e)
            {
                throw e;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UserFoodList()
        {
            sugarbattleEntities db = new sugarbattleEntities();
            FoodViewModel foodListModel = new FoodViewModel();
            List<FoodViewModel> foodViewList = new List<FoodViewModel>();
            List<TempFoodGain> tempFoodGains = db.TempFoodGain.ToList();

            foreach (var item in tempFoodGains)
            {
                int a  = (int)(Session["tempUserId"]);
                if (item.user_temp_id == a)
                {
                    if (item.Food.FoodType != null)
                    {
                        foodViewList.Add(new FoodViewModel
                        {
                            food_Id = item.Food.food_Id,
                            food_name = item.Food.food_name,
                            food_number = item.food_number,
                            quantity = item.Food.quantity,
                            sugar = Math.Round((decimal)((item.Food.sugar / 100) * item.food_number * item.Food.quantity), 2),
                            food_type_ID = item.Food.FoodType.food_type_ID,
                            type_description = item.Food.FoodType.type_description
                        });
                    }
                    else
                    {
                        foodViewList.Add(new FoodViewModel
                        {
                            food_Id = item.Food.food_Id,
                            food_name = item.Food.food_name,
                            food_number = item.food_number,
                            quantity = item.Food.quantity,
                            sugar = Math.Round((decimal)((item.Food.sugar / 100) * item.food_number * item.Food.quantity), 2),
                            food_type_ID = -1,
                            type_description = "temp"
                        });
                    }
                }
            }
            decimal totalFoodAmount = 0;
            decimal totalDrinkAmount = 0;
            decimal totalSugarAmount = 0;
            foreach (var item in foodViewList)
            {
                if (item.food_type_ID == 2)
                {
                    totalDrinkAmount += Math.Round((decimal)(item.food_number * item.quantity), 2);
                }
                else
                {
                    totalFoodAmount += Math.Round((decimal)(item.food_number * item.quantity), 2);;
                }
                totalSugarAmount += Math.Round((decimal)(item.sugar), 2);
            }
            ViewData["TotalFoodAmount"] = Math.Round((decimal)(totalFoodAmount / 1000), 2);
            ViewData["TotalDrinkAmount"] = Math.Round((decimal)(totalDrinkAmount / 1000), 2);
            ViewData["TotalSugarAmount"] = Math.Round((decimal)(totalSugarAmount), 2);
            if (foodViewList.Count == 0)
            {
                return View();
            }
            else
            {
                return View(foodViewList);
            }
            
        }

        [HttpGet]
        public ActionResult CalculateResult(string SugarTotalAmount)
        {
            sugarbattleEntities db = new sugarbattleEntities();
       
            ViewBag.SugarTotalAmount = SugarTotalAmount;
            ViewBag.ScoopNumber = (int)(decimal.Parse(SugarTotalAmount) / 15);
            ViewBag.CubeSugarNumber = (int)(decimal.Parse(SugarTotalAmount) / 4);
            ViewBag.JoggingNumber = (int)(decimal.Parse(SugarTotalAmount) / 20);
            ViewBag.AverageNumber = (int)(decimal.Parse(SugarTotalAmount) / 61);
            return PartialView();
        }

        [HttpPost]
        public JsonResult AddNewFood(string FoodName, string FoodAmount, string SugarAmount, string FoodNumber)
        {
            string result = "";
            if (FoodName.Trim() == "")
            {
                result = "Food Name could not be empty!";
            }
            else if (FoodName.Trim().Length > 50)
            {
                result = "Food Name should less than 50 characters!";
            }
            else if (FoodAmount.Trim() == "")
            {
                result = "Food Amount could not be empty!";
            }
            else if (FoodNumber.Trim() == "")
            {
                result = "Please Choose How much You eat!";
            }
            else if (SugarAmount.Trim() == "")
            {
                result = "Sugar Amount could not be empty!";
            }
            else if (int.Parse(FoodNumber.Trim()) == 0)
            {
                result = "Food number could not be empty!";
            }
            else if (decimal.Parse(SugarAmount.Trim()) > 100)
            {
                result = "Sugar Amount could not larger than 100g!";
            }
            else
            {
                sugarbattleEntities db = new sugarbattleEntities();
                List<Food> foodList = db.Food.Where(x => x.food_name == FoodName.Trim()).ToList();
                int userId = (int)Session["tempUserId"];
                if (foodList.Count == 0)
                {
                    Food food = new Food();
                    food.food_name = FoodName.Trim();
                    food.sugar = Math.Round(decimal.Parse(SugarAmount.Trim()), 2);
                    food.quantity = Math.Round(decimal.Parse(FoodAmount.Trim()), 2);
                    db.Food.Add(food);
                    db.SaveChanges();
                    int foodId = food.food_Id;
                    TempFoodGain tempFoodGain = new TempFoodGain();
                    tempFoodGain.food_Id = foodId;
                    tempFoodGain.user_temp_id = userId;
                    tempFoodGain.food_number = Math.Round(decimal.Parse(FoodNumber.Trim()), 2);
                    db.TempFoodGain.Add(tempFoodGain);
                    db.SaveChanges();
                }
                else
                {
                    TempFoodGain tempFoodGain = new TempFoodGain();
                    foreach (var item in foodList)
                    {
                        tempFoodGain = db.TempFoodGain.SingleOrDefault(x => x.food_Id == item.food_Id && x.user_temp_id == userId);
                        if (tempFoodGain != null)
                        {
                            break;
                        }
                    }

                    if (tempFoodGain == null)
                    {
                        Food food = new Food();
                        food.food_name = FoodName.Trim();
                        food.sugar = Math.Round(decimal.Parse(SugarAmount.Trim()), 2);
                        food.quantity = Math.Round(decimal.Parse(FoodAmount.Trim()), 2);
                        db.Food.Add(food);
                        db.SaveChanges();
                        int foodId = food.food_Id;
                        TempFoodGain tempFood = new TempFoodGain();
                        tempFood.food_Id = food.food_Id;
                        tempFood.food_number = Math.Round(decimal.Parse(FoodNumber.Trim()), 2);
                        tempFood.user_temp_id = userId;
                        db.TempFoodGain.Add(tempFood);
                        db.SaveChanges();
                    }
                    else
                    {
                        tempFoodGain.food_number += Math.Round(decimal.Parse(FoodNumber.Trim()), 2);
                        db.SaveChanges();
                    }
                }
                result = "success";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveFood()
        {
            sugarbattleEntities db = new sugarbattleEntities();

            int userId = (int)Session["user"];
            int tempUserId = (int)Session["tempUserId"];
            List<TempFoodGain> tempFoodGains = db.TempFoodGain.Where(x => x.user_temp_id == tempUserId).ToList();
            DateTime dateTime = DateTime.Now;
            foreach (var item in tempFoodGains)
            {
                UserFood userFood = new UserFood();
                userFood.FoodId = item.food_Id;
                userFood.FoodNumber = Math.Round((decimal)item.food_number, 2);
                userFood.UserId = userId;
                userFood.FoodDate = DateTime.Now;
                db.UserFood.Add(userFood);
                db.SaveChanges();
            }

            db.TempFoodGain.RemoveRange(tempFoodGains);
            db.SaveChanges();
            Session["tempUserId"] = null;

            return RedirectToAction("/ShowMyFoodList");
        }

        public ActionResult ShowMyFoodList()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "Account");
            } else
            {
                sugarbattleEntities db = new sugarbattleEntities();

                int userId = (int)Session["user"];



                List<UserFoodViewModel> UserFoodList = db.UserFood.Where(x => x.UserId == userId).GroupBy(y => y.FoodDate).Select(z => new UserFoodViewModel
                {
                    FoodDate = z.Key,
                    FoodAmountTotal = z.Sum(w => (decimal)(w.Food.quantity * w.FoodNumber)),
                    SugarAmountTotal = z.Sum(w => (decimal)(w.FoodNumber * w.Food.quantity * (w.Food.sugar / 100)))
                }).ToList();

                foreach (var item in UserFoodList)
                {
                    List<FoodViewModel> foodList = db.UserFood.Where(x => x.FoodDate == item.FoodDate && x.UserId == userId).Select(x => new FoodViewModel
                    {
                        food_Id = x.FoodId,
                        food_name = x.Food.food_name,
                        quantity = x.Food.quantity,
                        food_number = x.FoodNumber,
                        type_description = x.Food.FoodType.type_description,
                        sugar = Math.Round((decimal)((x.Food.sugar / 100) * x.FoodNumber * x.Food.quantity), 2),
                    }).ToList();
                    item.UserFoodList = foodList;
                }

                return View(UserFoodList);
            }
           
        }
    }
}