﻿using SugarBattle.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SugarBattle.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LogOff()
        {
            Session.Remove("user");
            Session.Remove("username");
            string url = (string)Session["url"];
            string[] routes = url.Split('/');
            return RedirectToAction(routes[1], routes[0]);
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            sugarbattleEntities db = new sugarbattleEntities();

            if (!ModelState.IsValid)
            {
                return View();
            } else
            {
                user user = db.user.SingleOrDefault(x => x.email == model.Email && x.password == model.Password);

                if (user == null)
                {
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
                } else
                {
                    Session["user"] = user.Id;
                    Session["username"] = user.username;
                    string url = (string)Session["url"];
                    if (url == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        string[] routes = url.Split('/');
                        DateTime nextWeek = DateTime.Now.AddDays(5);
                        List<UserLocation> userLocations = db.UserLocation.Where(x => x.UserId == user.Id && x.LocationStart.CompareTo(DateTime.Now) > 0 && x.LocationStart.CompareTo(nextWeek) < 0).OrderBy(y => y.LocationStart).ToList();
                        if (userLocations != null)
                        {
                            Session["locationNum"] = userLocations.Count();
                            Session["locations"] = userLocations;
                        } else
                        {
                            Session["locationNum"] = 0;
                            Session["locations"] = null;
                        }
                        
                        return RedirectToAction(routes[1], routes[0]);
                    }
                }
            }
        
        }

        // GET: Account
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            } else
            {
                
                sugarbattleEntities db = new sugarbattleEntities();

                user user = db.user.SingleOrDefault(x => x.email == model.Email);

                if (user != null)
                {
                    ModelState.AddModelError("", "This email has already been registered!");
                    return View(model);
                } else
                {
                    user = new user
                    {
                        email = model.Email,
                        password = model.Password,
                        username = model.username
                    };
                    db.user.Add(user);
                    db.SaveChanges();
                    int userId = user.Id;
                    Session["user"] = userId;
                    Session["username"] = user.username;

                    string url = (string)Session["url"];
                    string[] routes = url.Split('/');
                    DateTime nextWeek = DateTime.Now.AddDays(5);
                    List<UserLocation> userLocations = db.UserLocation.Where(x => x.UserId == user.Id && x.LocationStart.CompareTo(DateTime.Now) > 0 && x.LocationStart.CompareTo(nextWeek) < 0).OrderBy(y => y.LocationStart).ToList();
                    if (userLocations != null)
                    {
                        Session["locationNum"] = userLocations.Count();
                        Session["locations"] = userLocations;
                    }
                    else
                    {
                        Session["locationNum"] = 0;
                        Session["locations"] = null;
                    }
                    return RedirectToAction(routes[1], routes[0]);
                }
            }
        }
    }
}