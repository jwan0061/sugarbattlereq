﻿using Newtonsoft.Json;
using SugarBattle.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SugarBattle.Controllers
{
    public class LocationController : Controller
    {
        // GET: Location
        public ActionResult Index()
        {
            sugarbattleEntities db = new sugarbattleEntities();

            List<Location> locationList = db.Location.ToList();

            ViewData["GoCalendar"] = "false";

            return View(locationList);
        }

        public ActionResult GoIndex()
        {
            sugarbattleEntities db = new sugarbattleEntities();

            List<Location> locationList = db.Location.ToList();

            ViewData["GoCalendar"] = "true";

            return View("Index", locationList);
        }

        public ActionResult ShowWeather()
        {
            return PartialView("WeatherForecast");
        }

        public ActionResult UserCalendar()
        {
            sugarbattleEntities db = new sugarbattleEntities();

            if (Session["user"] != null)
            {
                int userId = (int)Session["user"];

                List<UserCalendarViewModel> userCalendarActivities = db.UserLocation.Where(x => x.UserId == userId).Select(y => new UserCalendarViewModel
                {
                    Id = y.Id,
                    LocationId = y.LocationId,
                    LocationName = y.Location.Name,
                    LocationStart = y.LocationStart,
                    LocationEnd = y.LocationEnd,
                    LocationDescription = y.Location.Description,
                    locationType = y.Location.locationType,
                    Description = y.Description,
                }).ToList();

                return PartialView("UserCalendar", userCalendarActivities);
            }
            return PartialView("UserCalendar", null);
        }

        public JsonResult ShowLocation(int LocationId)
        {
            if (Session["user"] == null)
            {
                return Json("false", JsonRequestBehavior.AllowGet);
            }
            else
            {
                sugarbattleEntities db = new sugarbattleEntities();

                Location location = db.Location.SingleOrDefault(x => x.Id == LocationId);
                UserCalendarViewModel model = new UserCalendarViewModel();
                model.LocationId = location.Id;
                model.LocationName = location.Name;
                
                model.LocationDescription = location.Description;
                return Json(model, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult EditActivity(int ID)
        {
            sugarbattleEntities db = new sugarbattleEntities();

            UserLocation userLocation = db.UserLocation.SingleOrDefault(x => x.Id == ID);

            ActivityEditViewModel model = new ActivityEditViewModel();
            model.Id = userLocation.Id;
            model.LocationId = userLocation.LocationId;
            model.LocationName = userLocation.Location.Name;
            model.LocationDescription = userLocation.Location.Description;
            model.LocationDate = userLocation.LocationStart.ToString("yyyy/M/d");
            model.LocationStart = userLocation.LocationStart.ToString("HH:mm");
            model.LocationEnd = userLocation.LocationEnd.ToString("HH:mm");
            model.Description = userLocation.Description;

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddToCalendar(string LocationId, string Desc, string Date, string Start, string End, string ActivityId)
        {
            sugarbattleEntities db = new sugarbattleEntities();

            if (ActivityId == "")
            {
                

                if (Date == "" || Start == "" || End == "")
                {
                    return Json("Please Choose the Right Date Time", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string startTime = Date + " " +  Start;
                    string endTime = Date + " " + End;

                    DateTime activityStart = DateTime.ParseExact(startTime, "yyyy/M/d H:mm", System.Globalization.CultureInfo.CurrentCulture);
                    DateTime activityEnd = DateTime.ParseExact(endTime, "yyyy/M/d H:mm", System.Globalization.CultureInfo.CurrentCulture);

                    if(DateTime.Compare(activityStart, activityEnd) >= 0)
                    {
                        return Json("Start TIme Must Earlier Than End Time", JsonRequestBehavior.AllowGet);
                    } else
                    {
                        UserLocation userLocation = new UserLocation();
                        userLocation.LocationId = int.Parse(LocationId);
                        userLocation.UserId = (int)Session["user"];
                        userLocation.LocationStart = activityStart;
                        userLocation.LocationEnd = activityEnd;
                        userLocation.Description = Desc;

                        db.UserLocation.Add(userLocation);
                        db.SaveChanges();
                    }
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
            } else
            {
                if (Date == "" || Start == "" || End == "")
                {
                    return Json("Please Choose the Right Date Time", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string startTime = Date + " " + Start;
                    string endTime = Date + " " + End;

                    DateTime activityStart = DateTime.ParseExact(startTime, "yyyy/M/d H:mm", System.Globalization.CultureInfo.CurrentCulture);
                    DateTime activityEnd = DateTime.ParseExact(endTime, "yyyy/M/d H:mm", System.Globalization.CultureInfo.CurrentCulture);

                    if (DateTime.Compare(activityStart, activityEnd) >= 0)
                    {
                        return Json("Start TIme Must Earlier Than End Time", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        int aid = int.Parse(ActivityId);
                        UserLocation userLocation = db.UserLocation.SingleOrDefault(x => x.Id == aid);
                        userLocation.LocationStart = activityStart;
                        userLocation.LocationEnd = activityEnd;
                        userLocation.Description = Desc;

                        db.SaveChanges();

                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                }
            }
        }

        public JsonResult CancelFromCalendar(string LocationId)
        {
            sugarbattleEntities db = new sugarbattleEntities();

            int id = int.Parse(LocationId);

            UserLocation userLocation = db.UserLocation.SingleOrDefault(x => x.Id == id);
            db.UserLocation.Remove(userLocation);
            db.SaveChanges();

            return Json("success", JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult FindNearParks(decimal lat, decimal lng)
        {
            sugarbattleEntities db = new sugarbattleEntities();

            List<Location> locations = db.Location.ToList();

            List<Location> nearlocations = new List<Location>();
            decimal max = 0;
            Location maxLocation = new Location();
            foreach (var item in locations)
            {
                decimal distance = Math.Abs(item.Latitude - lat) + Math.Abs(item.Longitude - lng);
                if (nearlocations.Count() < 10)
                {
                    if (distance > max)
                    {
                        max = distance;
                        maxLocation = item;
                    }
                    nearlocations.Add(item);
                }
                else
                {
                    if (distance < max)
                    {
                        nearlocations[nearlocations.IndexOf(maxLocation)] = item;
                        int tempMaxIndex = 0;
                        decimal tempMax = Math.Abs(nearlocations[0].Latitude - lat) + Math.Abs(nearlocations[0].Longitude - lng);
                        for (int i = 0; i < 10; i++)
                        {
                            decimal tempDistance = Math.Abs(nearlocations[i].Latitude - lat) + Math.Abs(nearlocations[i].Longitude - lng);
                            if (tempDistance > tempMax)
                            {
                                tempMaxIndex = i;
                                tempMax = tempDistance;
                            }
                        }
                        max = tempMax;
                        maxLocation = nearlocations[tempMaxIndex];
                    }
                }
            }

            return View("NearLocations", nearlocations);
        }
    }
}