﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Configuration;

namespace SugarBattle.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult FirstPage()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Disease()
        {
            return View();
        }

        public ActionResult FactDiagram()
        {
            return View();
        }

        public ActionResult Diagram1()
        {
            return PartialView();
        }

        public ActionResult Diagram2()
        {
            return PartialView();
        }

        public ActionResult Diagram3()
        {
            return PartialView();
        }

        public ActionResult ObesePage()
        {
            return View();
        }
    }
}