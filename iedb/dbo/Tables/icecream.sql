﻿CREATE TABLE [dbo].[icecream] (
    [brand]       NVARCHAR (50) NOT NULL,
    [Caloriecals] DECIMAL (18)  NULL,
    [Sugarg]      DECIMAL (18)  NULL,
    PRIMARY KEY CLUSTERED ([brand] ASC)
);

